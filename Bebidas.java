package arrayConObjetos;

import java.util.Arrays;

public class Bebidas {	
	private static int generaId=1;
	private int id;
	private double cantidad,precio;
	private String marca;
	
	public Bebidas(double cantidad, double precio, String marca) {
		id = generaId++;
		this.cantidad = cantidad;
		this.precio = precio;
		this.marca = marca;
	}

	//METODOS GET
	public int getId(){return id;}
	public double getCantidad(){return cantidad;}
	public double getPrecio(){return precio;}
	public String getMarca(){return marca;}
	//METODOS SET
	public void setCantidad(double cantidad){this.cantidad = cantidad;}	
	public void setPrecio(double precio){this.precio = precio;}
	public void setMarca(String marca){this.marca = marca;}
	//METODO toString
	public String toString() {
		return "Bebidas [id=" + id + ", cantidad=" + cantidad + ", precio=" + precio + ", marca=" + marca + "]";
	}
}

class AguaMineral extends Bebidas{
	private String procedencia;
	
	public AguaMineral(double cantidad, double precio, String marca, String procedencia) {
		super(cantidad, precio, marca);
		this.procedencia=procedencia;
	}
	
	//METODO GET
	public String getProcedencia(){return procedencia;}
	//METODO SET
	public void setProcedencia(String procedencia){this.procedencia = procedencia;}
	//METODO toString
	public String toString(){
		return "AguaMineral [procedencia=" + procedencia + "]";
	}
}

class BebidaAzucarada extends Bebidas{
	private int porcentaje;
	private boolean promocion;
	
	public BebidaAzucarada(double cantidad, double precio, String marca, int porcentaje, boolean promocion) {
		super(cantidad, precio, marca);
		this.porcentaje=porcentaje;
		this.promocion=promocion;
	}

	//METODOS GET
	public int getPorcentaje(){return porcentaje;}
	public boolean isPromocion(){return promocion;}
	public double getPrecio(){
		if(isPromocion())
			return super.getPrecio()*0.9;
		return super.getPrecio();
	}
	//METODOS SET
	public void setPorcentaje(int porcentaje){this.porcentaje = porcentaje;}
	public void setPromocion(boolean promocion){this.promocion = promocion;}
	//METODO toString
	public String toString() {
		return "BebidaAzucarada [porcentaje=" + porcentaje + ", promocion=" + promocion + "]";
	}
}

class Almacen{
	private Bebidas[][] estanteria; 
	
	//CONSTRUCTORES
	public Almacen(){estanteria = new Bebidas[3][3];}
	public Almacen(int f, int c){estanteria = new Bebidas[f][c];}
	
	//METODOS GET
	public int getCalcularPrecio(){
		return 0;
	}
	
	public void mostrarInformacion(int id) {
		for(int i=0;i<estanteria.length;i++){
			for(int j=0;j<estanteria[i].length;j++){
				if(estanteria[i][j]!=null){
					System.out.println(estanteria[i][j].toString());
				}
			}
		}
	}
	//METODOS SER
	public void agregarBebida(Bebidas b) {
		boolean encontrado=false;
		for(int i=0;i<estanteria.length && !encontrado;i++){
			for(int j=0;j<estanteria[i].length && !encontrado;j++){
				if(estanteria[i][j]==null){
					estanteria[i][j]=b;
					encontrado=true;
				}
				
				if(encontrado){
					System.out.println("Bebida añadida");
				}else{
					System.out.println("Estanteria llena");
				}
			}
		}
	}
	
	public void eliminarBebida(int id) {
		boolean encontrado=false;
		for(int i=0;i<estanteria.length && !encontrado;i++){
			for(int j=0;j<estanteria[i].length && !encontrado;j++){
				if(estanteria[i][j]!=null && estanteria[i][j].getId()==id){
					estanteria[i][j]=null;
					encontrado=true;
				}
				
				if(encontrado){
					System.out.println("Bebida elimiada");
				}else{
					System.out.println("No existe la bebida");
				}
			}
		}
	}
}